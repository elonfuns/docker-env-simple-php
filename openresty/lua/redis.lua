ngx.header.content_type = 'text/html';


local redis = require "resty.redis"
local red = redis:new()
red:set_timeout(1000)
local ok, err = red:connect("172.19.0.9", 6379)
if not ok then
    ngx.say("failed to connect: ", err)
end


local method = ngx.var.request_method
if method == "POST" then
    ngx.req.read_body()
    local authkey = ngx.req.get_body_data()
    ngx.say(authkey)
    ok, err = red:get(authkey)
elseif method == "GET" then
    local authkey = ngx.var.arg_auth
    ngx.say(authkey)
    ok, err = red:get(authkey)
end
ngx.say(ok)
if not ok then
    ngx.exit(ngx.HTTP_OK)
end
local ok, err = red:set_keepalive(10000, 100)
if not ok then
    ngx.say("failed to set keepalive: ", err)
end
ngx.exit(ngx.HTTP_OK)